<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "coches".
 *
 * @property int $id
 * @property string|null $marca
 * @property string|null $modelo
 * @property float|null $precio
 * @property string|null $fechaEntrada
 * @property string|null $foto
 * @property int|null $cilindrada
 */
class Coches extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coches';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'cilindrada'], 'integer'],
            [['precio'], 'number'],
            [['fechaEntrada'], 'safe'],
            [['marca', 'modelo', 'foto'], 'string', 'max' => 50],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'marca' => 'Marca',
            'modelo' => 'Modelo',
            'precio' => 'Precio',
            'fechaEntrada' => 'Fecha Entrada',
            'foto' => 'Foto',
            'cilindrada' => 'Cilindrada',
        ];
    }
}
