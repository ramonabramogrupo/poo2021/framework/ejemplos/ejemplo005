<?php
use yii\widgets\ListView;

?>

<?php
echo ListView::widget([
   'dataProvider' => $dataProvider,
   'itemView' =>'_ofertas',
    'itemOptions'=> ['class' => 'card'],
    'options' => ['class'=>'card-columns'],
    'layout' => "{items}"
]);
?>


