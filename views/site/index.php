<?php

/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title = 'Concesionario Leon';
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-8">

                <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

            </div>
            <div class="col-lg-4">
                <?= Html::img('@web/imgs/concesionario.jpg',[
                    'class' => 'img-fluid'
                ])?>
                
            </div>
        </div>

    </div>
</div>
