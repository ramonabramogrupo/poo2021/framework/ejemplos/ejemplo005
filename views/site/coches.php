<?php
use yii\grid\GridView;
use yii\helpers\Html;

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' =>[
        'id',
        'marca',
        'modelo',
        'precio',
        [
            'label'=>'foto',
            'format'=>'raw',
            'value' => function($data){
                $url = '@web/imgs/' . $data->foto;
                return Html::img($url,[
                    'class'=>'img-fluid',
                    'style'=>'width:300px'
                    ]); 
            }
        ],
        [
            'label'=>'Acciones',
            'format'=>'raw',
            'value' => function($data){
                $url = ['site/ver','id'=>$data->id];
                return Html::a('Ver mas...', $url, ['class' => 'btn btn-primary']); 
            }
        ],
                
    ]
]);

