<?php
use yii\widgets\DetailView;
use yii\helpers\Html;

echo DetailView::widget([
    'model' => $modelo,
    'attributes'=>[
        'id',
        'marca',
        'modelo',
        'precio',
        'fechaEntrada',
        'cilindrada',
        [
            'label'=>'foto',
            'format'=>'raw',
            'value' => function($data){
                $url = '@web/imgs/' . $data->foto;
                return Html::img($url,[
                    'class'=>'img-fluid',
                    'style'=>'width:300px'
                    ]); 
            }
        ],
    ]
]);
