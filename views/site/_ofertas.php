<?php
    use yii\helpers\Html;
?>
  <ul class="list-group list-group-flush">
    <li class="list-group-item">Marca: <?= $model->marca ?></li>
    <li class="list-group-item">Modelo: <?= $model->modelo ?></li>
    <li class="list-group-item">Precio: <?= $model->precio ?></li>
    <li class="list-group-item">
            <?= Html::a('Ver mas...', ['site/ver', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </li>
  </ul>

